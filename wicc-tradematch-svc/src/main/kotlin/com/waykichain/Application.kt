package com.waykichain

import com.waykichain.commons.util.BaseEnv
import org.springframework.boot.SpringApplication
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.ComponentScan

@SpringBootConfiguration
@ComponentScan(basePackages = arrayOf("com.waykichain"))
@EnableCaching
@EnableAutoConfiguration

open class Application

fun main(args: Array<String>){
    BaseEnv.init()
    val app = SpringApplication(Application::class.java)
    app.run(*args)
}

/*

@Component
class TradeCommandLineRunner: CommandLineRunner {

    override fun run(vararg args: String?) {
        tradeRequestService.onCancelRequest("1")
        //tradeSvc.run()
       // tradeEstimateService.onTxFailed("4232")
    }

    @Autowired lateinit var tradeRequestService: TradeRequestService
    @Autowired lateinit var tradeEstimateService: TradeEstimateService
    @Autowired
    lateinit var tradeSvc: TradeSvc

}*/
