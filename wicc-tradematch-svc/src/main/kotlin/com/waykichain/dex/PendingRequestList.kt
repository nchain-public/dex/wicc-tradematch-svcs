package com.waykichain.dex

import com.waykichain.dex.base.SpringContextAccessor
import com.waykichain.dex.base.dict.TradeRequestDirection
import com.waykichain.dex.base.dict.TradeRequestPriceType
import com.waykichain.dex.commons.biz.service.WiccTradeRequestService
import com.waykichain.dex.entity.domain.WiccTradeRequest
import com.waykichain.dex.ext.isNotMatchCompleted
import com.waykichain.dex.ext.isPending
import java.math.BigDecimal
import java.util.*

/**
 *  Created by yehuan on 2019/6/18
 */

class PendingRequestList private constructor() {

    private var list: List<WiccTradeRequest> = ArrayList()
    private var position = 0
    private var tradePairCode: String? = null
    private var direction:TradeRequestDirection? = null


    companion object {

        fun  valueOf(data: List<WiccTradeRequest>): PendingRequestList{
            val result = PendingRequestList()
            result.list = data
            if(data.isNotEmpty()){
                result.tradePairCode = data.first().tradePairCode
                result.direction = TradeRequestDirection.getByCode(data.first().tradeDirection)
            }

            return result
        }
    }

    fun findMaxPossiblePrice(): BigDecimal?{

        for( l in list){
            if(l.requestPriceType == TradeRequestPriceType.LIMIT_PRICE.code && l.isPending())
                return l.targetPrice
        }

        if(direction!!.code == TradeRequestDirection.BUY.code){
            return wiccTradeRequestService.findMaxBuyPriceRequest(tradePairCode!!)
        }
        else if ( direction!!.code == TradeRequestDirection.SELL.code){
            return wiccTradeRequestService.findMinSellPriceRequest(tradePairCode!!)
        }
        return null

    }

    fun isEmpty(): Boolean = list.isEmpty()

    fun getRequest(index: Int) = list.get(index)

    fun getPosition() = position

    fun currentElement(): WiccTradeRequest? {
        if(isEmpty())
            return null
        val req =  list.get(position)
        if(req.isNotMatchCompleted())
            return req
        return next()
    }

    private fun next(): WiccTradeRequest?{
        if(list.size == position+1)
            return null
        position++
        return currentElement()

    }

    val wiccTradeRequestService = SpringContextAccessor.getBean(WiccTradeRequestService::class.java)
}