package com.waykichain.dex

import com.waykichain.dex.repository.WiccTradePairRepository
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.Executors

/**
 *  Created by yehuan on 2019/6/19
 */

@Service
class TradeSvc:  DisposableBean{

    val executor = Executors.newFixedThreadPool(2)
    val tradeContextList = arrayListOf<MatchingContext>()


    fun run(){

        val pairs = wiccTradePairRepository.findAll()
        pairs.forEach {
            val context = MatchingContext.valueOf(it)
            tradeContextList.add(context)
            context.match()
            //executor.submit(Runnable { context.match() })

        }

    }

    fun stop(){
        tradeContextList.forEach {
            it.running = false
        }
    }

    override fun destroy() {
        executor.shutdown()
    }

    @Autowired lateinit var wiccTradePairRepository: WiccTradePairRepository

}