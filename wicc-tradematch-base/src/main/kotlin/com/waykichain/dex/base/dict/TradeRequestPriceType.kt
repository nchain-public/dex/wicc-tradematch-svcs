package com.waykichain.dex.base.dict

/**
 *  Created by yehuan on 2019/6/17
 */


enum class TradeRequestPriceType(var code: Int, var msg: String) {
    MARKET_PRICE(100, "市价单") ,
    LIMIT_PRICE(200, "限价单") ;

    companion object {
        fun getList(): Array<TradeRequestPriceType> {
            return TradeRequestPriceType.values()
        }
    }
}