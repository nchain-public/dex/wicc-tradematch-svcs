package com.waykichain.dex.base

import java.text.SimpleDateFormat

/**
 *  Created by yehuan on 2019/5/8
 */


val yyyyMMddFormat = SimpleDateFormat("yyyy-MM-dd")


val GLOBAL_MINIMUM_TRADE_UNIT = 0.0001.toBigDecimal()

val GLOBAL_DECIMAL_DIGITS: Int =  4