package com.waykichain.dex.base.consts

/**
 *  Created by yehuan on 2019/4/19
 */

object DexRedisKey{

    val DEX_CHAIN_HEIGHT_REDIS_KEY = "DEX_CHAIN_HEIGHT_REDIS_KEY"
    
    val DEX_MATCH_LOCK_REDIS_KEY = "DEX_MATCH_LOCK_REDIS_KEY"
}