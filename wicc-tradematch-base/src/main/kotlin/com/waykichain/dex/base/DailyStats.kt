package com.waykichain.dex.base

import java.util.Date
import java.util.Calendar

/**
 *  Created by yehuan on 2019/5/22
 */

interface DailyStats{

    fun stats(days: Int) {

        for (i in 1..days) {
            val date = dateAdd(Date(), -i, Calendar.DATE)
            val dateString = yyyyMMddFormat.format(date)
            dailyStats(dateString)
        }

    }

    private fun dateAdd( date: Date,  value: Int,  unit: Int): Date{
        val c = Calendar.getInstance()
        c.time = date
        c.add(unit, value)
        return c.time
    }

    fun dailyStats(dateString: String)
}