package com.waykichain.dex.base.dict

/**
 *  Created by yehuan on 2019/6/18
 */

enum class TradeRequestStatus(val code: Int, val cancelable: Boolean,val description: String){

    PENDING(100, true, "挂单中"),

    MATCHED(200, true,"撮合完成"),

    SETTLED(400, false,"核算完成"),

    CANCELED(900, false,"取消"),

    INVALID(1000, false,"无效的") ;


    companion object {

        fun cancelableStatuCodes(): List<Int>{
            val statuses = TradeRequestStatus.values().filter { it.cancelable == true }.toList()

            val list = arrayListOf<Int>()
            statuses.forEach {
                list.add(it.code)
            }
            return list

        }


    }

}