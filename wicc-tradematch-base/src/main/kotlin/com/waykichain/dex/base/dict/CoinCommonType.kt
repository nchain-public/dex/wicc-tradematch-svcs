package com.waykichain.dex.base.dict

enum class CoinStatus(val code: Int, val msg: String) {
    ACTIVE(1, "上架"),
    IN_ACTIVE(0, "下架")
}

enum class AccountStatus(val code: Int, val msg: String) {
    NORMAL(100, "正常状态"),
    RESERVED(900, "冻结状态");
}

enum class TxType(val code: Int, val msg: String) {
    DEPOSIT(100, "充币"),
    WITHDRAW(700, "提币")
}
