package com.waykichain.dex.base.dict

/**
 *  Created by yehuan on 2019/6/18
 */

enum class TradeDetailStatus(val code: Int, val description: String){


    MATCHED(200, "撮合完成"),

    SETTLING(300, "结算中"),

    SETTLE_SUCCESSED(400, "核算完成"),

    SETTLE_FIALED(900, "取消")

}