package com.waykichain.dex.base.dict

/**
 *  Created by yehuan on 2019/6/17
 */

enum class TradeRequestDirection(var code: Int, var msg: String) {

    BUY(100,  "买"),
    SELL(200,  "卖");

    companion object {

        val map = TradeRequestDirection.values().associateBy { it.code  }
        fun getByCode(code: Int) = map[code]
        fun getList(): Array<TradeRequestDirection> {
            return TradeRequestDirection.values()
        }
    }
}

