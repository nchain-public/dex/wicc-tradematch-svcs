package com.waykichain.dex.base;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class QuerydslPageable implements Pageable {

    private  int pageSize;
    private  int offset ;
    private Sort sort ;

    public QuerydslPageable(int pageSize , int pageIndex, Sort sort){
        this.sort = sort;
        this.pageSize = pageSize ;
        this.offset = (pageIndex -1)* pageSize ;
    }
    @Override
    public int getPageNumber() {
        return 0 ;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public int getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        return sort ;
    }

    @Override
    public Pageable next() {
        return null;
    }

    @Override
    public Pageable previousOrFirst() {
        return null;
    }

    @Override
    public Pageable first() {
        return null;
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }
}
