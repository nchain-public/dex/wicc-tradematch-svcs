package com.waykichain.task.service

import com.waykichain.dex.entity.domain.ChartKlineLog
import com.waykichain.dex.entity.domain.WiccTradeDetail
import com.waykichain.task.beans.ChartKlineLogPO
import com.waykichain.task.beans.ChartKlineLogVO
import com.waykichain.task.beans.DataTablePageVO
import com.waykichain.task.beans.GenKlineCond

/**
 * Created by yinjun
 * @date 2018/4/2
 */
interface ChartKlineLogXservice {

    /**
     * query kline
     */
    fun list(po: ChartKlineLogPO): DataTablePageVO<ChartKlineLogVO>

    /**
     * generate kline of Minute
     *      if trades is empty -> return null
     * @param tradePairCode The trade_pair_code
     * @param trades The set of stats of trade_pair_code
     */
    fun genMinKline(tradePairCode: String, trades: List<WiccTradeDetail>): ChartKlineLog?

    /**
     * generate non-1min kline
    at com.xxl.job.core.rpc.netcom.NetComClientProxy$1.invoke(NetComClientProxy.java:57)
    at com.sun.proxy.$Proxy136.registry(Unknown Source)
    at com.xxl.job.core.thread.ExecutorRegistryThread$1.run(ExecutorRegistryThread.java:57)
    at java.lang.Thread.run(Thread.java:748)
    2019-06-27 14:57:53.764 ERROR Thread-19  com.xxl.job.core.rpc.netcom.jetty.client.JettyClient - Read timed out
    java.net.SocketTimeoutException: Read timed out
    at java.net.SocketInputStream.socketRead0(Native Method)
    at java.net.SocketInputStream.socketRead(SocketInputStream.java:116)
    at java.net.SocketInputStream.read(SocketInputStream.java:171)
    at java.net.SocketInputStream.read(SocketInputStream.java:141)
    at org.apache.http.impl.io.SessionInputBufferImpl.streamRead(SessionInputBufferImpl.java:137)
    at org.apache.http.impl.io.SessionInputBufferImpl.fillBuffer(SessionInputBufferImpl.java:153)
    at org.apache.http.impl.io.SessionInputBufferImpl.readLine(SessionInputBufferImpl.java:282)
    at org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(DefaultHttpResponseParser.java:138)
    at org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(DefaultHttpResponseParser.java:56)
    at org.apache.http.impl.io.AbstractMessageParser.parse(AbstractMessageParser.java:259)
    at org.apache.http.impl.DefaultBHttpClientConnection.receiveResponseHeader(DefaultBHttpClientConnection.java:163)
    at org.apache.http.impl.conn.CPoolProxy.receiveResponseHeader(CPoolProxy.java:165)
    at org.apache.http.protocol.HttpRequestExecutor.doReceiveResponse(HttpRequestExecutor.java:273)
    at org.apache.http.protocol.HttpRequestExecutor.execute(HttpRequestExecutor.java:125)
    at org.apache.http.impl.execchain.MainClientExec.execute(MainClientExec.java:272)
    at org.apache.http.impl.execchain.ProtocolExec.execute(ProtocolExec.java:185)
    at org.apache.http.impl.execchain.RedirectExec.execute(RedirectExec.java:111)
    at org.apache.http.impl.client.InternalHttpClient.doExecute(InternalHttpClient.java:185)
    at org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:83)
    at org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:108)
    at com.xxl.job.core.util.HttpClientUtil.postRequest(HttpClientUtil.java:60)
    at com.xxl.job.core.rpc.netcom.jetty.client.JettyClient.send(JettyClient.java:29)
    at com.xxl.job.core.rpc.netcom.NetComClientProxy$1.invoke(NetComClientProxy.java:57)
    at com.sun.proxy.$Proxy136.registry(Unknown Source)
    at com.xxl.job.core.thread.ExecutorRegistryThread$1.run(ExecutorRegistryThread.java:57)
    at java.lang.Thread.run(Thread.java:748)
    2019-06-27 16:02:44.696 ERROR Thread-19  com.xxl.job.core.rpc.netcom.jetty.client.JettyClient - Read timed out
    java.net.SocketTimeoutException: Read timed out
    at java.net.SocketInputStream.socketRead0(Native Method)
     */
    fun genNonMinKline(cond: GenKlineCond): ChartKlineLog?
}