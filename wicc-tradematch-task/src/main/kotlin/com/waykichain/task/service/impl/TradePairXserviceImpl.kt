package com.waykichain.task.service.impl

import com.waykichain.dex.base.dict.CoinStatus
import com.waykichain.dex.commons.biz.service.WiccTradePairService
import com.waykichain.dex.entity.domain.WiccTradePair
import com.waykichain.task.service.WiccTradePairXservice
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TradePairXserviceImpl : WiccTradePairXservice {

    override fun findActive(): List<WiccTradePair> {
        logger.debug("find on_shelf trade_pair")
        return tradePairService.find(CoinStatus.ACTIVE.code)
    }

    private val logger = LoggerFactory.getLogger(javaClass)
    @Autowired
    private lateinit var tradePairService: WiccTradePairService
}