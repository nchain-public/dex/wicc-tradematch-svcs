package com.waykichain.task.service

import com.waykichain.dex.entity.domain.WiccTradePair

interface WiccTradePairXservice {

    fun findActive(): List<WiccTradePair>
}