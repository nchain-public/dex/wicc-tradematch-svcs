package com.waykichain.task.jobs

import com.waykichain.commons.util.DateUtils
import com.waykichain.dex.base.dict.PeriodType
import com.waykichain.dex.commons.biz.service.ChartKlineLogService
import com.waykichain.dex.commons.biz.service.WiccTradeDetailService
import com.waykichain.dex.entity.domain.ChartKlineLog
import com.waykichain.dex.entity.domain.WiccTradeDetail
import com.waykichain.task.service.ChartKlineLogXservice
import com.waykichain.task.service.WiccTradePairXservice
import com.waykichain.utils.BizDateUtils
import com.xxl.job.core.biz.model.ReturnT
import com.xxl.job.core.handler.IJobHandler
import com.xxl.job.core.handler.annotation.JobHandler
import com.xxl.job.core.log.XxlJobLogger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Created by yinjun
 * @date 2018/4/4
 */
@Service
//@JobHandler(value = "klineJobHandler")
class KlineJobHandler : IJobHandler() {

    private val ONE_MIN_MS = PeriodType.ONE_MIN.sec * 1000

    override fun execute( param: String?): ReturnT<String> {
        var  params = param!!.split(",")


        val currentMs = System.currentTimeMillis()
        printJobLog(DateUtils.dateFormat(DateUtils.getCurrentDate()))
        params.forEach { symbol ->


            // check trade_pair_code
            if (symbol !in tradePairXservice.findActive().map { it.code }) {
                printJobLog("not support this symbol $symbol")
                return@forEach
            }

            // get latest eos
            printJobLog("start $symbol")
            var latestEos = chartKlineLogService.getLatestEos(symbol, PeriodType.ONE_MIN.code)

            if (latestEos == null) { // not exist eos
                val trade = tradeService.findFirst(symbol)// get 1st stats
                trade?.apply { latestEos = genKline(BizDateUtils.getMinStart(trade.tradedAtMs),symbol) }
            }
            //
            latestEos?.apply {
                val times = BizDateUtils.getMinStart(currentMs.minus(ONE_MIN_MS).minus(eos)).div(ONE_MIN_MS).toInt()
                var beg = eos.plus(ONE_MIN_MS)
                IntRange(1, times).forEach {
                    printJobLog("normal generate")
                    genKline(beg,symbol)
                    logger.debug("current beg eos is: $beg")
                    beg = beg.plus(ONE_MIN_MS)
                }
            }

            val cost = (System.currentTimeMillis() - currentMs).div(1000)
            printJobLog("cost $cost sec")
        }
        return ReturnT.SUCCESS
    }
    /**
     * inner fun
     */
    fun genKline(beg: Long, symbol: String): ChartKlineLog? {
        val trades = tradeService.findByTradedAtMs(beg, beg.plus(ONE_MIN_MS), symbol)
        logger.debug("generate k-line of symbol: $symbol")
        return chartKlineLogXservice.genMinKline(symbol, trades)
    }
    fun printJobLog(log: String) {
        XxlJobLogger.log("XXL-JOB, klineCountJobHandler: $log")
    }
    private val logger = LoggerFactory.getLogger(javaClass)
    @Autowired
    private lateinit var chartKlineLogService: ChartKlineLogService
    @Autowired
    private lateinit var chartKlineLogXservice: ChartKlineLogXservice
    @Autowired
    private lateinit var tradeService: WiccTradeDetailService
    @Autowired
    private lateinit var tradePairXservice: WiccTradePairXservice
}