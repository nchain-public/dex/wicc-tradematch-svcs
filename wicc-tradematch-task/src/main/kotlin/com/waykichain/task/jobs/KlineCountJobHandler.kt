package com.waykichain.task.jobs

import com.waykichain.commons.util.DateUtils
import com.waykichain.dex.base.dict.PeriodType
import com.waykichain.dex.commons.biz.service.ChartKlineLogService
import com.waykichain.task.beans.GenKlineCond
import com.waykichain.task.service.ChartKlineLogXservice
import com.waykichain.task.service.WiccTradePairXservice
import com.waykichain.utils.BizDateUtils
import com.xxl.job.core.biz.model.ReturnT
import com.xxl.job.core.handler.IJobHandler
import com.xxl.job.core.handler.annotation.JobHandler
import com.xxl.job.core.log.XxlJobLogger

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
//@JobHandler(value = "klineCountJobHandler")
class KlineCountJobHandler : IJobHandler() {

    override fun execute(para: String?): ReturnT<String> {

        var params = para!!.split(",")
        val start = System.currentTimeMillis()


        // verify params
        val periodCode = params.first()
        if (periodCode == null) {
            return ReturnT.FAIL
        }
        if (periodCode !in PeriodType.codes()) {
            return ReturnT.FAIL
        }
        val PERIOD_MS = PeriodType.getByCode(periodCode)!!.sec.times(1000)

        // traverse symbol

        val codes = tradePairXservice.findActive().map { it.code }
        (1..params.lastIndex).forEach { index ->
            val symbol = params[index]
            if (symbol !in codes) {
                return@forEach
            }

            // get latest kline by period_code and symbol
            val latestKline = chartKlineLogService.getLatestEos(symbol!!, periodCode)
            var beg: Long
            if (latestKline == null) { // not exist k-line
                // get 1st k-line of 1min
                val oneMinKline = chartKlineLogService.getFirstKline()
                oneMinKline ?: return@forEach
                beg = oneMinKline.eos
            } else {
                beg = latestKline.eos.plus(PERIOD_MS)
            }

            // natural circle
            beg = beg.minus(beg.rem(PERIOD_MS))
            logger.debug("beg: ${DateUtils.dateFormat(BizDateUtils.getDate(beg))}")

            val times = (start - beg).div(PERIOD_MS).toInt()
            IntRange(1, times).forEach {
                val cond = GenKlineCond(beg, beg.plus(PERIOD_MS), symbol, periodCode)
                chartKlineLogXservice.genNonMinKline(cond)
                beg += PERIOD_MS
                logger.debug("current beg_eos is :$beg")
            }
        }
        return ReturnT.SUCCESS
    }

    private val logger = LoggerFactory.getLogger(javaClass)
    @Autowired
    private lateinit var chartKlineLogService: ChartKlineLogService
    @Autowired
    private lateinit var chartKlineLogXservice: ChartKlineLogXservice
    @Autowired
    private lateinit var tradePairXservice: WiccTradePairXservice
}