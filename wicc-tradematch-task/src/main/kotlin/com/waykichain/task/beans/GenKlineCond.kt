package com.waykichain.task.beans

/**
 * Created by yinjun
 * @date 2018/4/8
 */
data class GenKlineCond(
        val begEos: Long,
        val endEos: Long,
        val symbol: String,
        val periodCode: String)