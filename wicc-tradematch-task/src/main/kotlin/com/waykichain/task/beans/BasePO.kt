package com.waykichain.task.beans

import io.swagger.annotations.ApiModelProperty

/**
 * Created by yinjun
 * @date 2018/4/2
 */
open class BasePO {

    @ApiModelProperty(hidden = true)
    open var customerId: Long? = null

    @ApiModelProperty("分页INDEX (开始为1)")
    var pageIndex: Int? = null

    @ApiModelProperty("分页SIZE ")
    var pageSize: Int? = null
}