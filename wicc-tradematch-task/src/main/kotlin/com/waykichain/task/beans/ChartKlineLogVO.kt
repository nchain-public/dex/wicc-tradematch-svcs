package com.waykichain.task.beans

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

/**
 * Created by yinjun
 * @date 2018/4/2
 */
@ApiModel
class ChartKlineLogVO {

    @ApiModelProperty("收盘价")
    var closePrice: java.math.BigDecimal? = null

    @ApiModelProperty("成交币数")
    var count: BigDecimal? = null
    var createdAt: java.util.Date? = null

    var eos: Long? = null

    @ApiModelProperty("k-line时间点")
    var eosDate: java.util.Date? = null

    @ApiModelProperty("最高价")
    var highPrice: java.math.BigDecimal? = null
    var id: Long? = null

    @ApiModelProperty("最低价")
    var lowPrice: java.math.BigDecimal? = null

    @ApiModelProperty("开盘价格")
    var openPrice: java.math.BigDecimal? = null

    @ApiModelProperty("时间间隔")
    var periodCode: String? = null

    @ApiModelProperty("币对符号")
    var symbol: String? = null
    var updatedAt: java.util.Date? = null

    @ApiModelProperty("成交额 [美元]")
    var volAmount: java.math.BigDecimal? = null

    @ApiModelProperty("成交笔数")
    var orderCount: Int? = null
}