package com.waykichain.task.beans

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import org.hibernate.validator.constraints.NotBlank
import javax.validation.constraints.NotNull

/**
 * Created by yinjun
 * @date 2018/4/2
 */
@ApiModel
class ChartKlineLogPO : BasePO() {

    @ApiModelProperty("开始时间")
    @NotNull(message = "输入开始时间")
    var eosDateBeg: Long? = null

    @ApiModelProperty("结束时间")
    @NotNull(message = "输入结束时间")
    var eosDateEnd: Long? = null

    @ApiModelProperty("时间间隔[1min,5min,...,1day]")
    @NotNull(message = "输入时间间隔")
    @NotBlank(message = "时间间隔不能为blank")
    var periodCode: String? = null

    @ApiModelProperty("币种交易符号[btcusdt,bchbtc]")
    @NotNull(message = "symbol can not null")
    @NotBlank(message = "symbol can not blank")
    var symbol: String? = null
}