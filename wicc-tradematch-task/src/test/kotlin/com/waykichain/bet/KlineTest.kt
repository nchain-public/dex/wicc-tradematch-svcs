package com.waykichain.bet

import com.waykichain.task.jobs.KlineCountJobHandler
import com.waykichain.task.jobs.KlineJobHandler
import org.junit.Test
import org.junit.runner.RunWith
import org.mybatis.spring.annotation.MapperScan
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner


@RunWith(SpringRunner::class)
@SpringBootTest
@MapperScan("com/waykichain/dex/commons/entity/mapper")
class KlineTest {


    @Test
    fun testKlineGen(){
        klineJobHandler.execute("WICC-WUSD")
    }

    @Test
    fun testKlineCount(){
        klineCountJobHandler.execute("15min,WICC-WUSD")
    }

    @Autowired lateinit var klineCountJobHandler: KlineCountJobHandler
    @Autowired lateinit var klineJobHandler: KlineJobHandler

}