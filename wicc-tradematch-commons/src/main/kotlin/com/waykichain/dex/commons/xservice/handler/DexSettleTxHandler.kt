package com.waykichain.dex.commons.xservice.handler

import com.alibaba.fastjson.JSONObject
import com.waykichain.chain.vo.tx.DexSettleOrderTx
import com.waykichain.dex.base.dict.DexTransactionType
import com.waykichain.dex.commons.xservice.TradeSettleXService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 *  Created by yehuan on 2019/7/11
 */

@Service
class DexSettleTxHandler: DexTransactionHandler<DexSettleOrderTx>(){


    override fun getSupportedTxType(): DexTransactionType  = DexTransactionType.DEX_SETTLE_TX

    override fun doHandle(tx:DexSettleOrderTx) {
        val hash = tx.hash
        tradeSettleXService.onTxSuccess(hash)
    }

    @Autowired lateinit var tradeSettleXService: TradeSettleXService
}

