package com.waykichain.dex.commons.redis.impl

import com.waykichain.biz.redis.repository.impl.ValueCacheRedisRepository
import com.waykichain.dex.commons.redis.RedisLockService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RedisLockServiceImpl: RedisLockService {
    override fun lock(lockKey: String, interupt: Boolean): Boolean {
        var result = valueCache.setNxEx(lockKey,"1", 10)

        if(!interupt)
            return result

        var start = System.currentTimeMillis()
        while (!result){
            result = valueCache.setNxEx(lockKey,"1", 10)
            //防止死循环
            var end = System.currentTimeMillis()
            if((end - start)/1000 > 500)
                break
        }

        return result

    }

    override fun unlock(lockKey: String): Boolean {
        valueCache.delete(lockKey)
        return true
    }


    @Autowired lateinit var valueCache: ValueCacheRedisRepository

}