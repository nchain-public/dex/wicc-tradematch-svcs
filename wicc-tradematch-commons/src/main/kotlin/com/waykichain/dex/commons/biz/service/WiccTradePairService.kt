package com.waykichain.dex.commons.biz.service

import com.waykichain.dex.entity.domain.WiccTradePair

interface WiccTradePairService {
    fun getById(id:Long): WiccTradePair?

    fun save(wiccTradePair:WiccTradePair): WiccTradePair

    fun find(onShelf: Int): List<WiccTradePair>
}
