package com.waykichain.dex.commons.biz.service.impl

import com.waykichain.dex.commons.biz.service.WiccBlockService
import com.waykichain.dex.entity.domain.WiccBlock
import com.waykichain.dex.repository.WiccBlockRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class WiccBlockServiceImpl: WiccBlockService {

    override fun getById(id:Long): WiccBlock? {
        return wiccBlockRepository.findOne(id)
    }

    override fun save(wiccBlock:WiccBlock): WiccBlock {
        return wiccBlockRepository.saveAndFlush(wiccBlock)
    }

    @Autowired lateinit var wiccBlockRepository: WiccBlockRepository
}
