package com.waykichain.dex.commons

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.stereotype.Component

/**
 *  Created by yehuan on 2019/7/1
 */
class LogPostBeanProcessor: BeanPostProcessor{

    override fun postProcessBeforeInitialization(bean: Any?, beanName: String?): Any {
        var fs =     bean!!.javaClass.declaredFields

        fs.forEach {

            val l = it.getAnnotationsByType(Autowired::class.java)
            if( l != null && l.isNotEmpty())
                println("注入结果== " +(it.get(bean) != null))

        }


        return bean!!
    }

    override fun postProcessAfterInitialization(bean: Any?, beanName: String?): Any {

        var fs =     bean!!.javaClass.declaredFields

        fs.forEach {

            val l = it.getAnnotationsByType(Autowired::class.java)
            if( l != null && l.isNotEmpty())
                println("注入结果1111111== " +(it.get(bean) != null))

        }

        return  bean!!

    }

}

