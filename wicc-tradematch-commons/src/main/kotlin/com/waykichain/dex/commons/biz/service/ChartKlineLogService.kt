package com.waykichain.dex.commons.biz.service

import com.querydsl.core.types.dsl.BooleanExpression
import com.waykichain.dex.entity.domain.ChartKlineLog
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest

interface ChartKlineLogService {
    fun getById(id:Long): ChartKlineLog?

    /**
     * @param cond queryDsl condition
     * @param pr page request
     */
    fun find(cond: BooleanExpression, pr: PageRequest?): Page<ChartKlineLog>

    fun save(chartKlineLog: ChartKlineLog): ChartKlineLog

    /**
     * get the latest by symbol
     * @param symbol coin_pair symbol
     * @param periodCode period_code see PeriodType
     * @see com.zetafin.coinx.commons.biz.dict.coin.PeriodType
     */
    fun getLatestEos(symbol: String, periodCode: String): ChartKlineLog?

    /**
     * get the first k-line
     */
    fun getFirstKline(): ChartKlineLog?

    /**
     * Note: eosBeg <= eos < eosEnd
     * @param eosBeg the beginning eos
     * @param eosEnd the ended eos exclude eosEnd
     * @param periodCode period_code
     */
    fun getKlineList(eosBeg: Long, eosEnd: Long, periodCode: String,symbol: String): List<ChartKlineLog>
}
