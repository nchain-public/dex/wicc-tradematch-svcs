package com.waykichain.dex.commons.biz.service

import com.waykichain.dex.entity.domain.WiccDexTransaction

interface WiccDexTransactionService {
    fun getById(id:Long): WiccDexTransaction?

    fun save(wiccDexTransaction:WiccDexTransaction): WiccDexTransaction

    fun findRollbackTransaction(startHeight: Int, endHeight: Int): List<WiccDexTransaction>
}
