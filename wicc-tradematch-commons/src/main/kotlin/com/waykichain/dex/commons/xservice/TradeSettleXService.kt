package com.waykichain.dex.commons.xservice

import com.waykichain.dex.base.dict.TradeStatus
import com.waykichain.dex.commons.biz.service.WiccTradeDetailService
import com.waykichain.dex.commons.biz.service.WiccTradeRequestService
import com.waykichain.dex.commons.biz.service.WiccTradeService
import com.waykichain.dex.entity.domain.WiccTrade
import com.waykichain.dex.ext.needRollback
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 *  Created by yehuan on 2019/6/19
 */

@Service
class TradeSettleXService{


    val logger = LoggerFactory.getLogger(javaClass)

    fun  onTxFailed(tradeId: Long){
        val trade = wiccTradeService.getById(tradeId)!!
        onTradeFailed(trade)
    }

    fun onTxFailed(txHash: String){
        val trade =  wiccTradeService.findByTxHash(txHash)
        onTradeFailed(trade)
    }

    fun onTxSuccess(txHash: String){
        val trade = wiccTradeService.findByTxHash(txHash)

        if( trade.status == TradeStatus.SETTLING.code){
            wiccTradeService.onSettleSuccess(trade)
            wiccTradeDetailService.onSettleSuccess(trade)
            wiccTradeRequestService.onSettleSuccess( trade)

        }else{
            logger.error("结算订单未在待确认状态，不能确认")
            return
        }

    }

    private  fun onTradeFailed(trade: WiccTrade){

        if(trade.needRollback()){
            wiccTradeService.onSettleFailed(trade)
            wiccTradeDetailService.onSettleFailed(trade)
            wiccTradeRequestService.onSettleFailed(trade)
        }

    }


    @Autowired lateinit var wiccTradeRequestService: WiccTradeRequestService
    @Autowired lateinit var wiccTradeDetailService: WiccTradeDetailService
    @Autowired lateinit var wiccTradeService: WiccTradeService

}