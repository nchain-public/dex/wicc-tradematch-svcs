package com.waykichain.dex.commons.biz.service

import com.waykichain.dex.entity.domain.ChartDepthLog

interface ChartDepthLogService {
    fun getById(id:Long): ChartDepthLog?

    fun save(chartDepthLog:ChartDepthLog): ChartDepthLog
}
