package com.waykichain.dex.commons.redis

interface RedisLockService{

    fun lock(lockKey: String,interupt: Boolean): Boolean

    fun unlock(lockKey: String): Boolean
}