package com.waykichain.dex.commons.biz.service.impl

import com.waykichain.dex.commons.biz.service.WiccDexTransactionService
import com.waykichain.dex.entity.domain.QWiccDexTransaction
import com.waykichain.dex.entity.domain.WiccDexTransaction
import com.waykichain.dex.repository.WiccDexTransactionRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort

@Service
class WiccDexTransactionServiceImpl: WiccDexTransactionService {

    override fun findRollbackTransaction(startHeight: Int, endHeight: Int): List<WiccDexTransaction> {

        val predicate = QWiccDexTransaction.wiccDexTransaction.txHeight.between(startHeight,endHeight)
        val sort = Sort(Sort.Direction.DESC, "transactedAtMs")
        return wiccDexTransactionRepository.findAll(predicate,sort).toList()
    }

    override fun getById(id:Long): WiccDexTransaction? {
        return wiccDexTransactionRepository.findOne(id)
    }

    override fun save(wiccDexTransaction:WiccDexTransaction): WiccDexTransaction {
        return wiccDexTransactionRepository.saveAndFlush(wiccDexTransaction)
    }

    @Autowired lateinit var wiccDexTransactionRepository: WiccDexTransactionRepository
}
