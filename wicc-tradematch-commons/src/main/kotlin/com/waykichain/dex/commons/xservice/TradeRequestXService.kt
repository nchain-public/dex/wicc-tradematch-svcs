package com.waykichain.dex.commons.xservice

import com.waykichain.dex.base.dict.TradeDetailStatus
import com.waykichain.dex.base.dict.TradeRequestStatus
import com.waykichain.dex.commons.biz.service.WiccTradeDetailService
import com.waykichain.dex.commons.biz.service.WiccTradeRequestService
import com.waykichain.dex.entity.domain.WiccTradeRequest
import com.waykichain.dex.ext.cancelable
import com.waykichain.dex.ext.requestIsMatching
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 *  Created by yehuan on 2019/6/19
 */

@Component
class TradeRequestXService{

    val logger = LoggerFactory.getLogger(javaClass)

    fun onNewRequest(){

    }

    fun onCancelRequest( requestTxHash: String){

        var req = wiccTradeRequestService.getValidRequestByTxHash(requestTxHash)?: return

        if( req.cancelable()){
            req = cancelRequest(req)
            if(req.requestIsMatching()){
                cancelTrade(req)
            }
        }else{
            logger.error("挂单已经交易完成，不能被取消")
        }

    }

    private fun cancelTrade(req: WiccTradeRequest){
        val  tradeDetails = wiccTradeDetailService.findByTradeRequest(req)

        val canceledTradeHash = hashSetOf<Long>()
        for(detail in tradeDetails){
            if(detail.status == TradeDetailStatus.SETTLING.code && !canceledTradeHash.contains(detail.tradeOrderId)){
                tradeSettleXService.onTxFailed(detail.tradeOrderId)
                canceledTradeHash.add(detail.tradeOrderId)
            }
        }
    }

    private fun cancelRequest(req: WiccTradeRequest): WiccTradeRequest{
        req.status = TradeRequestStatus.CANCELED.code
        return wiccTradeRequestService.save(req)

    }

    @Autowired lateinit var wiccTradeDetailService: WiccTradeDetailService
    @Autowired lateinit var wiccTradeRequestService: WiccTradeRequestService
    @Autowired lateinit var tradeSettleXService: TradeSettleXService

}