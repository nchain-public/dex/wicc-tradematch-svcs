package com.waykichain.dex.commons.biz.service

import com.waykichain.dex.entity.domain.WiccTradeRequestLog

interface WiccTradeRequestLogService {
    fun getById(id:Long): WiccTradeRequestLog?

    fun save(wiccTradeRequestLog:WiccTradeRequestLog): WiccTradeRequestLog
}
