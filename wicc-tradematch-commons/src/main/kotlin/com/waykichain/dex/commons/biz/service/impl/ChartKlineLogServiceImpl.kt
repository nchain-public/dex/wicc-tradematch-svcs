package com.waykichain.dex.commons.biz.service.impl

import com.querydsl.core.types.dsl.BooleanExpression
import com.waykichain.dex.base.dict.PeriodType
import com.waykichain.dex.commons.biz.service.ChartKlineLogService
import com.waykichain.dex.entity.domain.ChartKlineLog
import com.waykichain.dex.entity.domain.QChartKlineLog
import com.waykichain.dex.repository.ChartKlineLogRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.querydsl.QSort

@Service
class ChartKlineLogServiceImpl: ChartKlineLogService {

    override fun getById(id:Long): ChartKlineLog? {
        return chartKlineLogRepository.findOne(id)
    }
    override fun find(cond: BooleanExpression, pr: PageRequest?): Page<ChartKlineLog> {
        return chartKlineLogRepository.findAll(cond, pr)
    }

    override fun save(chartKlineLog: ChartKlineLog): ChartKlineLog {
        logger.debug("saveOrUpdate kline ${chartKlineLog.symbol}")
        return chartKlineLogRepository.save(chartKlineLog)
    }

    override fun getLatestEos(symbol: String, periodCode: String): ChartKlineLog? {
        logger.debug("get latest eos by: $symbol")
        val predicate = QChartKlineLog.chartKlineLog.symbol.eq(symbol)
                .and(QChartKlineLog.chartKlineLog.periodCode.eq(periodCode))
        val eosSort = QSort(QChartKlineLog.chartKlineLog.eos.desc())
        val pr = PageRequest(0, 1, eosSort)
        val result = find(predicate, pr)
        return result.content.firstOrNull()
    }

    override fun getFirstKline(): ChartKlineLog? {
        logger.debug("get 1st k-line")
        val predicate = QChartKlineLog.chartKlineLog.periodCode.eq(PeriodType.ONE_MIN.code)
        val eosSort = QSort(QChartKlineLog.chartKlineLog.eos.asc())
        val pr = PageRequest(0, 1, eosSort)
        val result = find(predicate, pr)
        return result.content.firstOrNull()
    }

    override fun getKlineList(eosBeg: Long, eosEnd: Long, periodCode: String,symbol: String): List<ChartKlineLog> {
        logger.debug("eos[$eosBeg,$eosEnd] and symbol $periodCode")
        val predicate = QChartKlineLog.chartKlineLog.periodCode.eq(periodCode)
                .and(QChartKlineLog.chartKlineLog.eos.between(eosBeg.plus(1), eosEnd)).and(QChartKlineLog
                        .chartKlineLog.symbol.eq(symbol))
        return chartKlineLogRepository.findAll(predicate).toList()
    }

    private val logger = LoggerFactory.getLogger(javaClass)
    @Autowired lateinit var chartKlineLogRepository: ChartKlineLogRepository
}
