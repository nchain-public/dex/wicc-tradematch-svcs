package com.waykichain.dex.commons.xservice.handler

import com.alibaba.fastjson.JSONObject
import com.waykichain.dex.base.dict.DexTransactionType
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Component

/**
 *  Created by yehuan on 2019/7/11
 */

@Component
class DexTransactionHandlerManager: ApplicationContextAware{

    private val handlers = hashMapOf<String, DexTransactionHandler<*>>()

    private fun  registHandler(type: DexTransactionType, handler: DexTransactionHandler<*>){
        handlers.put(type.description, handler)
    }

    fun handle(tx: String?){
        tx?: return
        val json = JSONObject.parseObject(tx)
        val txtype = json.getString("txtype")?:return
        val handler = handlers.get(txtype)?:return
        handler.doHandle(tx)

    }

    override fun setApplicationContext(applicationContext: ApplicationContext) {

        val handlers = applicationContext.getBeansOfType(DexTransactionHandler::class.java).values
        handlers.forEach {
            registHandler(it.getSupportedTxType(), it)
        }

    }












}