package com.waykichain.dex.commons.biz.service

import com.waykichain.dex.entity.domain.WiccTrade
import com.waykichain.dex.entity.domain.WiccTradeDetail
import com.waykichain.dex.entity.domain.WiccTradePair
import com.waykichain.dex.entity.domain.WiccTradeRequest
import java.math.BigDecimal

interface WiccTradeRequestService: SettleResultService {
    fun getById(id:Long): WiccTradeRequest?

    fun save(requests: List<WiccTradeRequest>): List<WiccTradeRequest>

    fun save(wiccTradeRequest:WiccTradeRequest): WiccTradeRequest

    fun getLastPendingBuyRequest(pair: WiccTradePair):List<WiccTradeRequest>

    fun getLastPendingSellRequest(pair: WiccTradePair): List<WiccTradeRequest>

    fun getValidRequestByTxHash(txHash: String): WiccTradeRequest?

    fun rollback(wiccTradeDetail: WiccTradeDetail)


    fun undoRequest(txHash: String)

    fun findMaxBuyPriceRequest(tradePairCode: String): BigDecimal?

    fun findMinSellPriceRequest(tradePairCode: String): BigDecimal?


}
