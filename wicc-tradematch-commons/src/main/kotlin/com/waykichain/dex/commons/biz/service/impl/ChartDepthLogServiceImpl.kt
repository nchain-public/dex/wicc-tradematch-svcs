package com.waykichain.dex.commons.biz.service.impl

import com.waykichain.dex.commons.biz.service.ChartDepthLogService
import com.waykichain.dex.entity.domain.ChartDepthLog
import com.waykichain.dex.repository.ChartDepthLogRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class ChartDepthLogServiceImpl: ChartDepthLogService {

    override fun getById(id:Long): ChartDepthLog? {
        return chartDepthLogRepository.findOne(id)
    }

    override fun save(chartDepthLog:ChartDepthLog): ChartDepthLog {
        return chartDepthLogRepository.saveAndFlush(chartDepthLog)
    }

    @Autowired lateinit var chartDepthLogRepository: ChartDepthLogRepository
}
