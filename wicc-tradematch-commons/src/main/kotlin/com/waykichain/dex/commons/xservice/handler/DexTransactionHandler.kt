package com.waykichain.dex.commons.xservice.handler

import com.alibaba.fastjson.JSONObject
import com.waykichain.dex.base.dict.DexTransactionType
import java.lang.reflect.ParameterizedType

/**
 *  Created by yehuan on 2019/7/11
 */
abstract class DexTransactionHandler<T>{


    open fun doHandle(tx: String){

        val type =  (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<T>
        val json = JSONObject.parseObject(tx, type)
        doHandle(json)
    }

    abstract fun doHandle(tx: T)


    abstract fun getSupportedTxType(): DexTransactionType



}