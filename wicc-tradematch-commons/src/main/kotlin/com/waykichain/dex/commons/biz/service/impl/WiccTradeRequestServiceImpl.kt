package com.waykichain.dex.commons.biz.service.impl

import com.waykichain.dex.base.dict.*
import com.waykichain.dex.base.sumByBigDecimal
import com.waykichain.dex.commons.biz.service.SettleResultService
import com.waykichain.dex.commons.biz.service.WiccTradeDetailService
import com.waykichain.dex.commons.biz.service.WiccTradeRequestLogService
import com.waykichain.dex.commons.biz.service.WiccTradeRequestService
import com.waykichain.dex.entity.domain.*
import com.waykichain.dex.ext.isMarketBuyRequest
import com.waykichain.dex.ext.rollback
import com.waykichain.dex.repository.WiccTradeRequestRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal

@Service
class WiccTradeRequestServiceImpl: WiccTradeRequestService, SettleResultService {


    override fun onSettleSuccess(trade: WiccTrade) {

        val details =  wiccTradeDetailService.findByTradeId(trade.id)
        val requestHashSet = hashSetOf<String>()
        details.forEach {
            requestHashSet.add(it.buyOrderTxHash)
            requestHashSet.add(it.sellOrderTxHash)
        }

        requestHashSet.forEach{
            val request = getValidRequestByTxHash(it)!!
            if(request.status == TradeRequestStatus.MATCHED.code && allDetailSettled(request)){
                request.status = TradeRequestStatus.SETTLED.code
                save(request)
            }
        }
    }


    private fun allDetailSettled(req: WiccTradeRequest): Boolean{
        val details = wiccTradeDetailService.findByTradeRequest(req)
        val settledDetail = details.filter{ it.status == TradeDetailStatus.SETTLE_SUCCESSED.code}.toList()

        if(req.isMarketBuyRequest()){
            return req.limitDealCoinAmount == settledDetail.sumByBigDecimal { it.tradeMoney }
        }else{
            return req.amount == settledDetail.sumByBigDecimal { it.tradeAmount }
        }

    }

    override fun onSettleFailed(trade: WiccTrade) {
        val  details = wiccTradeDetailService.findByTradeId(trade.id)

        details.forEach {
            rollback(it)
        }
    }

    override fun findMaxBuyPriceRequest(tradePairCode: String): BigDecimal? {

        val req = wiccTradeRequestRepository
                .findMaxBuyPriceRequest(TradeRequestDirection.BUY.code, tradePairCode, TradeRequestStatus.PENDING.code,TradeRequestPriceType.LIMIT_PRICE.code).firstOrNull()?: return null
        return req.targetPrice
    }

    override fun findMinSellPriceRequest(tradePairCode: String): BigDecimal? {

        val req = wiccTradeRequestRepository
                .findMinSellPriceRequest(TradeRequestDirection.SELL.code, tradePairCode, TradeRequestStatus.PENDING.code,TradeRequestPriceType.LIMIT_PRICE.code).firstOrNull()?: return null
        return req.targetPrice
    }


    override fun undoRequest(txHash: String) {

        val predicate = QWiccTradeRequest.wiccTradeRequest.txHash.eq(txHash)
        val req  = wiccTradeRequestRepository.findAll(predicate).first()
        req.status = TradeRequestStatus.INVALID.code
        req.actualTradeAmount = BigDecimal.ZERO
        save(req)
    }



    override fun getValidRequestByTxHash(txHash: String): WiccTradeRequest? {
        return wiccTradeRequestRepository.findAll(QWiccTradeRequest.wiccTradeRequest.txHash.eq(txHash)).firstOrNull { it.status != TradeRequestStatus.INVALID.code }
    }

    override fun rollback(wiccTradeDetail: WiccTradeDetail) {
        rollback(wiccTradeDetail.buyOrderTxHash, wiccTradeDetail)
        rollback(wiccTradeDetail.sellOrderTxHash, wiccTradeDetail)
    }

    private fun rollback( tradeRequestTxhash: String, tradeDetail: WiccTradeDetail){
        val request = getValidRequestByTxHash(tradeRequestTxhash)?: return
        request.rollback(tradeDetail)
        save(request)
    }

    override fun getById(id:Long): WiccTradeRequest? {
        return wiccTradeRequestRepository.findOne(id)
    }

    override fun save(wiccTradeRequest:WiccTradeRequest): WiccTradeRequest {

        wiccTradeRequestlog(wiccTradeRequest)
        return wiccTradeRequestRepository.saveAndFlush(wiccTradeRequest)
    }

    override fun save(requests: List<WiccTradeRequest>): List<WiccTradeRequest> {
        return wiccTradeRequestRepository.save(requests)
    }


    override fun getLastPendingBuyRequest(pair: WiccTradePair): List<WiccTradeRequest> {
        return wiccTradeRequestRepository.getPendingBuyRequest(TradeRequestDirection.BUY.code, pair.code, TradeRequestStatus.PENDING.code)
    }

    override fun getLastPendingSellRequest(pair: WiccTradePair): List<WiccTradeRequest> {
        return wiccTradeRequestRepository.getPendingSellRequest(TradeRequestDirection.SELL.code, pair.code, TradeRequestStatus.PENDING.code)
    }

    private fun wiccTradeRequestlog(wiccTradeRequest: WiccTradeRequest){

        if(wiccTradeRequest.id == null) return
        val oldReq = getById(wiccTradeRequest.id)?: return
        val wiccTradeRequestLog = WiccTradeRequestLog()
        wiccTradeRequestLog.requestId = wiccTradeRequest.id
        wiccTradeRequestLog.fromActualAmount = oldReq.actualTradeAmount
        wiccTradeRequestLog.toActualAmount = wiccTradeRequest.actualTradeAmount
        wiccTradeRequestLog.fromStatus = oldReq.status
        wiccTradeRequestLog.toStatus = wiccTradeRequest.status
        wiccTradeRequestLogService.save(wiccTradeRequestLog)

    }

    @Autowired lateinit var wiccTradeRequestLogService: WiccTradeRequestLogService
    @Autowired lateinit var wiccTradeDetailService: WiccTradeDetailService
    @Autowired lateinit var wiccTradeRequestRepository: WiccTradeRequestRepository
}
