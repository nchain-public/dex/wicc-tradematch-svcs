package com.waykichain.dex.commons.biz.service

import com.waykichain.dex.base.dict.TradeDetailStatus
import com.waykichain.dex.entity.domain.WiccTrade
import com.waykichain.dex.entity.domain.WiccTradeDetail
import com.waykichain.dex.entity.domain.WiccTradeRequest

interface WiccTradeDetailService: SettleResultService {
    fun getById(id:Long): WiccTradeDetail?

    fun save(wiccTradeDetail:WiccTradeDetail): WiccTradeDetail

    fun updateStatus(tradeId: Long, tradeDetailStatus: TradeDetailStatus)

    fun  findByTradeId( tradeId: Long):List<WiccTradeDetail>

    fun findByTradeRequest(req: WiccTradeRequest): List<WiccTradeDetail>


    override fun onSettleSuccess(trade: WiccTrade) {
        updateStatus(trade.id, TradeDetailStatus.SETTLE_SUCCESSED)
    }

    override fun onSettleFailed(trade: WiccTrade) {
        updateStatus(trade.id, TradeDetailStatus.SETTLE_FIALED)
    }


    /**
     * get the first stats by trade_pair_code
     */
    fun findFirst(tradePairCode: String): WiccTradeDetail?

    fun findByTradedAtMs(eomsBeg: Long, eomsEnd: Long, tradePairCode: String): List<WiccTradeDetail>
}
