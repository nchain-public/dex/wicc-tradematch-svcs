package com.waykichain.dex.config

import com.waykichain.commons.util.BaseEnv

object Environment{

         @JvmField
        val MYSQL_URL = BaseEnv.env("MYSQL_URL", "jdbc:mysql://127.0.0.1:13306/wicc-dextrade?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&useSSL=false")
        @JvmField
        val MYSQL_DRIVER = BaseEnv.env("MYSQL_DRIVER", "com.mysql.jdbc.Driver")
        @JvmField
        val MYSQL_USERNAME = BaseEnv.env("MYSQL_USERNAME", "root")
        @JvmField
        val MYSQL_PASSWORD = BaseEnv.env("MYSQL_PASSWORD", "yehuan@123")
        @JvmField
        val MYSQL_INITIALSIZE = BaseEnv.env("MYSQL_INITIALSIZE", 10)
        @JvmField
        val MYSQL_MIN_IDLE = BaseEnv.env("MYSQL_MIN_IDLE", 5)
        @JvmField
        val MYSQL_MAX_ACTIVE = BaseEnv.env("MYSQL_MAX_ACTIVE", 20)

        @JvmField
        val DEX_SETTLE_TX_SUBMITTER_ADDRESS = BaseEnv.env("DEX_SETTLE_TX_SUBMITTER_ADDRESS", "")

}