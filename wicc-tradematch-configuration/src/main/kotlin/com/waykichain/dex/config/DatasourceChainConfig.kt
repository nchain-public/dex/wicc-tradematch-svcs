
package com.waykichain.dex.config


import com.alibaba.druid.pool.DruidDataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import java.sql.SQLException
import javax.persistence.EntityManager
import javax.sql.DataSource

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef="entityManagerFactoryChain",
        transactionManagerRef="transactionManagerChain",
        basePackages= arrayOf("com.waykichain.dex.repository")) //设置Repository所在位置
open class DatasourceChainConfig {

    private val logger = org.slf4j.LoggerFactory.getLogger(javaClass)

    @Bean
    open fun dataSourceChain(): DataSource {

        val datasource = DruidDataSource()

        datasource.url = Environment.MYSQL_URL
        datasource.username = Environment.MYSQL_USERNAME
        datasource.password = Environment.MYSQL_PASSWORD
        datasource.driverClassName = Environment.MYSQL_DRIVER

        //configuration
        datasource.initialSize = Environment.MYSQL_INITIALSIZE
        datasource.minIdle = Environment.MYSQL_MIN_IDLE
        datasource.maxActive = Environment.MYSQL_MAX_ACTIVE
        datasource.maxWait = 5000
        datasource.timeBetweenEvictionRunsMillis = 10000
        datasource.minEvictableIdleTimeMillis = 30000
        datasource.isTestWhileIdle = true
        datasource.isTestOnBorrow = false
        datasource.isTestOnReturn = false
        datasource.isPoolPreparedStatements = true
        datasource.maxPoolPreparedStatementPerConnectionSize = 20
        datasource.setConnectionProperties("druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000")
        datasource.validationQuery = "select now()"

        try {
            datasource.setFilters("stat,wall,log4j")
        } catch (e: SQLException) {
            logger.error("druid configuration initialization filter", e)
        }

        return datasource
    }


    @Bean(
            "entityManagerChain"
    )
    open fun entityManager(builder: EntityManagerFactoryBuilder): EntityManager {
        return entityManagerFactory(builder).getObject().createEntityManager()
    }


    @Bean("entityManagerFactoryChain")
    open fun entityManagerFactory(builder: EntityManagerFactoryBuilder): LocalContainerEntityManagerFactoryBean {
        return builder
                .dataSource(dataSourceChain())
                .properties(getVendorProperties(dataSourceChain()))
                .packages("com.waykichain.dex.entity.domain") //设置实体类所在位置
                .persistenceUnit("PersistenceUnit")
                .build()
    }

    @Autowired
    private val jpaProperties: JpaProperties? = null

    private fun getVendorProperties(dataSource: DataSource): Map<String, String> {
        if(jpaProperties == null)
            return HashMap<String,String>()

        return jpaProperties.getHibernateProperties(dataSource)
    }


    @Bean("transactionManagerChain")
    open fun transactionManager(builder: EntityManagerFactoryBuilder): PlatformTransactionManager {
        return JpaTransactionManager(entityManagerFactory(builder).getObject())
    }

}
