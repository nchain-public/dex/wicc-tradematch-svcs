package com.waykichain.dex.repository

import com.waykichain.dex.entity.domain.WiccTrade
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface WiccTradeRepository : JpaRepository<WiccTrade, Long>,
    QueryDslPredicateExecutor<WiccTrade>
