package com.waykichain.dex.repository

import com.waykichain.dex.entity.domain.ChartKlineLog
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface ChartKlineLogRepository : JpaRepository<ChartKlineLog, Long>,
    QueryDslPredicateExecutor<ChartKlineLog>
