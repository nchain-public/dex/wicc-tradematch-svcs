package com.waykichain.dex.repository

import com.waykichain.dex.entity.domain.WiccTradeRequest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface WiccTradeRequestRepository : JpaRepository<WiccTradeRequest, Long>,
    QueryDslPredicateExecutor<WiccTradeRequest>
{

    @Query("select * from wicc_trade_request where trade_direction=?1 and trade_pair_code=?2 and status=?3 order by target_price desc, id asc limit 3000", nativeQuery = true)
    fun getPendingBuyRequest(direction: Int, pairCode: String, status: Int): List<WiccTradeRequest>

    @Query("select * from wicc_trade_request where trade_direction=?1 and trade_pair_code=?2 and status=?3 order by target_price asc,  id asc limit 3000", nativeQuery = true)
    fun getPendingSellRequest(direction: Int, pairCode: String, status: Int): List<WiccTradeRequest>

    @Query("select * from wicc_trade_request where trade_direction=?1 and trade_pair_code=?2 and status=?3 and request_price_type=?4 order by target_price desc limit 1",nativeQuery = true)
    fun findMaxBuyPriceRequest(direction: Int, pairCode: String, status: Int, priceType: Int): List<WiccTradeRequest>

    @Query("select * from wicc_trade_request where trade_direction=?1 and trade_pair_code=?2 and status=?3 and request_price_type=?4 order by target_price asc limit 1", nativeQuery = true)
    fun findMinSellPriceRequest(direction: Int, pairCode: String, status: Int, priceType: Int): List<WiccTradeRequest>
}