package com.waykichain.dex.repository

import com.waykichain.dex.entity.domain.WiccTradePair
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface WiccTradePairRepository : JpaRepository<WiccTradePair, Long>,
    QueryDslPredicateExecutor<WiccTradePair>
