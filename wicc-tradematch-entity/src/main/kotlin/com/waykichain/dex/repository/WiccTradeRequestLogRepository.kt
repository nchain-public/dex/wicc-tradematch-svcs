package com.waykichain.dex.repository

import com.waykichain.dex.entity.domain.WiccTradeRequestLog
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface WiccTradeRequestLogRepository : JpaRepository<WiccTradeRequestLog, Long>,
    QueryDslPredicateExecutor<WiccTradeRequestLog>
