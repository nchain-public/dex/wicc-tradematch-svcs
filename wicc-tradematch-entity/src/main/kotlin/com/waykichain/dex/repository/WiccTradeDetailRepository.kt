package com.waykichain.dex.repository

import com.waykichain.dex.entity.domain.WiccTradeDetail
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.querydsl.QueryDslPredicateExecutor
import javax.transaction.Transactional

interface WiccTradeDetailRepository : JpaRepository<WiccTradeDetail, Long>,
    QueryDslPredicateExecutor<WiccTradeDetail>{

    @Transactional
    @Modifying
    @Query("update wicc_trade_detail set status=?2 where trade_order_id=?1", nativeQuery = true)
    fun updateStatus(tradeId: Long, status: Int)
}
