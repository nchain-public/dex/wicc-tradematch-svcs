package com.waykichain.dex.exception

import com.waykichain.dex.entity.domain.WiccTradeRequest
import java.lang.Exception

/**
 *  Created by yehuan on 2019/6/24
 */

class RemainAmountZeroException(val request:WiccTradeRequest): Exception()