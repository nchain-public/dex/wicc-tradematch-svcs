package com.waykichain.dex.ext

import com.waykichain.dex.base.GLOBAL_DECIMAL_DIGITS
import com.waykichain.dex.base.GLOBAL_MINIMUM_TRADE_UNIT
import com.waykichain.dex.base.dict.TradeRequestDirection
import com.waykichain.dex.base.dict.TradeRequestPriceType
import com.waykichain.dex.base.dict.TradeRequestStatus
import com.waykichain.dex.entity.domain.WiccTradeDetail
import com.waykichain.dex.entity.domain.WiccTradeRequest
import java.math.BigDecimal

/**
 *  Created by yehuan on 2019/6/22
 */

fun WiccTradeRequest.isMarketPriceRequest()   = requestPriceType == TradeRequestPriceType.MARKET_PRICE.code

fun WiccTradeRequest.isLimitPriceRequest()    = requestPriceType == TradeRequestPriceType.LIMIT_PRICE.code

fun WiccTradeRequest.isPending()              = status == TradeRequestStatus.PENDING.code

fun WiccTradeRequest.requestIsMatching()      = actualTradeAmount > BigDecimal.ZERO

fun WiccTradeRequest.requstHasNoMatch()       =  isPending() && actualTradeAmount == BigDecimal.ZERO

fun WiccTradeRequest.isBuyRequest()           = tradeDirection == TradeRequestDirection.BUY.code

fun WiccTradeRequest.isSellRequest()          =  tradeDirection == TradeRequestDirection.SELL.code

fun WiccTradeRequest.isMarketBuyRequest() = isMarketPriceRequest() && isBuyRequest()

fun WiccTradeRequest.cancelable(): Boolean{
    return status in TradeRequestStatus.cancelableStatuCodes()
}


fun WiccTradeRequest.isNotMatchCompleted()    = !isMatchCompleted()

fun WiccTradeRequest.remainDealMoney(): BigDecimal{
    if(isMarketBuyRequest())
        return limitDealCoinAmount.subtract(actualDealCoinAmount)
    throw UnsupportedOperationException(" marketPrice and buy direction  tradeRequest be supported only ")
}

fun WiccTradeRequest.remainTradeAmount(price: BigDecimal? = BigDecimal.ZERO): BigDecimal   {

    var result: BigDecimal = BigDecimal.ZERO
    if(isMarketBuyRequest()){

        if(price == null || price == BigDecimal.ZERO)
            throw IllegalArgumentException("the value of price is illegal, you must provide a price that more than zero")

        val a1 = remainDealMoney().divide(price, GLOBAL_DECIMAL_DIGITS, BigDecimal.ROUND_DOWN)
        if(a1 <= BigDecimal.ZERO)
            return  GLOBAL_MINIMUM_TRADE_UNIT

        val a2 = a1 + GLOBAL_MINIMUM_TRADE_UNIT

        val delta1 = remainDealMoney() - a1.multiply(price)
        val delta2 = a2.multiply(price) - remainDealMoney()

        result = if(delta1 < delta2) a1 else a2

    }else{
        result =  amount.subtract(actualTradeAmount)
    }

    return result
}

fun WiccTradeRequest.isMatchCompleted(): Boolean{

    if( status != TradeRequestStatus.PENDING.code)
        return true

    if(isMarketBuyRequest())
        return actualDealCoinAmount >= limitDealCoinAmount

    return actualTradeAmount >= amount

}

fun WiccTradeRequest.modifyAfterMatch( detail: WiccTradeDetail){
    actualTradeAmount = actualTradeAmount.add(detail.tradeAmount)
    actualDealCoinAmount = actualDealCoinAmount.add(detail.tradeMoney)

    if(isMatchCompleted())
        status = TradeRequestStatus.MATCHED.code
}

fun WiccTradeRequest.rollback(tradeDetail: WiccTradeDetail){
    actualTradeAmount = actualTradeAmount.subtract(tradeDetail.tradeAmount)
    actualDealCoinAmount = actualDealCoinAmount.subtract(tradeDetail.tradeMoney)
    if(status == TradeRequestStatus.MATCHED.code)
        status = TradeRequestStatus.PENDING.code
}

