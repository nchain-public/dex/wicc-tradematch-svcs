package com.waykichain.dex.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * WiccTradeDetail is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class WiccTradeDetail implements Serializable {

    @Column("buy_order_tx_hash")
    private String buyOrderTxHash;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("id")
    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column("sell_order_tx_hash")
    private String sellOrderTxHash;

    @Column("status")
    private Integer status;

    @Column("trade_amount")
    private java.math.BigDecimal tradeAmount;

    @Column("traded_at_ms")
    private Long tradedAtMs;

    @Column("trade_money")
    private java.math.BigDecimal tradeMoney;

    @Column("trade_order_id")
    private Long tradeOrderId;

    @Column("trade_pair_code")
    private String tradePairCode;

    @Column("trade_price")
    private java.math.BigDecimal tradePrice;

    @Column("updated_at")
    private java.util.Date updatedAt;

    public String getBuyOrderTxHash() {
        return buyOrderTxHash;
    }

    public void setBuyOrderTxHash(String buyOrderTxHash) {
        this.buyOrderTxHash = buyOrderTxHash;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSellOrderTxHash() {
        return sellOrderTxHash;
    }

    public void setSellOrderTxHash(String sellOrderTxHash) {
        this.sellOrderTxHash = sellOrderTxHash;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public java.math.BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(java.math.BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Long getTradedAtMs() {
        return tradedAtMs;
    }

    public void setTradedAtMs(Long tradedAtMs) {
        this.tradedAtMs = tradedAtMs;
    }

    public java.math.BigDecimal getTradeMoney() {
        return tradeMoney;
    }

    public void setTradeMoney(java.math.BigDecimal tradeMoney) {
        this.tradeMoney = tradeMoney;
    }

    public Long getTradeOrderId() {
        return tradeOrderId;
    }

    public void setTradeOrderId(Long tradeOrderId) {
        this.tradeOrderId = tradeOrderId;
    }

    public String getTradePairCode() {
        return tradePairCode;
    }

    public void setTradePairCode(String tradePairCode) {
        this.tradePairCode = tradePairCode;
    }

    public java.math.BigDecimal getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(java.math.BigDecimal tradePrice) {
        this.tradePrice = tradePrice;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}

