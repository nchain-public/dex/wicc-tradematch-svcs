package com.waykichain.dex.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * WiccDexTransaction is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class WiccDexTransaction implements Serializable {

    @Column("id")
    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column("transacted_at_ms")
    private Long transactedAtMs;

    @Column("tx_hash")
    private String txHash;

    @Column("tx_height")
    private Integer txHeight;

    @Column("tx_type")
    private Integer txType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTransactedAtMs() {
        return transactedAtMs;
    }

    public void setTransactedAtMs(Long transactedAtMs) {
        this.transactedAtMs = transactedAtMs;
    }

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    public Integer getTxHeight() {
        return txHeight;
    }

    public void setTxHeight(Integer txHeight) {
        this.txHeight = txHeight;
    }

    public Integer getTxType() {
        return txType;
    }

    public void setTxType(Integer txType) {
        this.txType = txType;
    }

}

