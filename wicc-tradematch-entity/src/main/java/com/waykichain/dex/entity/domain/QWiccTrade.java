package com.waykichain.dex.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QWiccTrade is a Querydsl query type for WiccTrade
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QWiccTrade extends com.querydsl.sql.RelationalPathBase<WiccTrade> {

    private static final long serialVersionUID = 2040372047;

    public static final QWiccTrade wiccTrade = new QWiccTrade("wicc_trade");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath dealCoinSymbol = createString("dealCoinSymbol");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath marketCoinSymbol = createString("marketCoinSymbol");

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final NumberPath<java.math.BigDecimal> tradeAmount = createNumber("tradeAmount", java.math.BigDecimal.class);

    public final NumberPath<Integer> tradeDetailCount = createNumber("tradeDetailCount", Integer.class);

    public final NumberPath<java.math.BigDecimal> tradeMoney = createNumber("tradeMoney", java.math.BigDecimal.class);

    public final StringPath tradePairCode = createString("tradePairCode");

    public final StringPath txHash = createString("txHash");

    public final NumberPath<Integer> txHeight = createNumber("txHeight", Integer.class);

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<WiccTrade> primary = createPrimaryKey(id);

    public QWiccTrade(String variable) {
        super(WiccTrade.class, forVariable(variable), "null", "wicc_trade");
        addMetadata();
    }

    public QWiccTrade(String variable, String schema, String table) {
        super(WiccTrade.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QWiccTrade(String variable, String schema) {
        super(WiccTrade.class, forVariable(variable), schema, "wicc_trade");
        addMetadata();
    }

    public QWiccTrade(Path<? extends WiccTrade> path) {
        super(path.getType(), path.getMetadata(), "null", "wicc_trade");
        addMetadata();
    }

    public QWiccTrade(PathMetadata metadata) {
        super(WiccTrade.class, metadata, "null", "wicc_trade");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(11).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(dealCoinSymbol, ColumnMetadata.named("deal_coin_symbol").withIndex(6).ofType(Types.VARCHAR).withSize(32));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(marketCoinSymbol, ColumnMetadata.named("market_coin_symbol").withIndex(5).ofType(Types.VARCHAR).withSize(32));
        addMetadata(status, ColumnMetadata.named("status").withIndex(10).ofType(Types.INTEGER).withSize(10));
        addMetadata(tradeAmount, ColumnMetadata.named("trade_amount").withIndex(7).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(tradeDetailCount, ColumnMetadata.named("trade_detail_count").withIndex(9).ofType(Types.INTEGER).withSize(10));
        addMetadata(tradeMoney, ColumnMetadata.named("trade_money").withIndex(8).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(tradePairCode, ColumnMetadata.named("trade_pair_code").withIndex(4).ofType(Types.VARCHAR).withSize(64));
        addMetadata(txHash, ColumnMetadata.named("tx_hash").withIndex(2).ofType(Types.VARCHAR).withSize(128));
        addMetadata(txHeight, ColumnMetadata.named("tx_height").withIndex(3).ofType(Types.INTEGER).withSize(10));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(12).ofType(Types.TIMESTAMP).withSize(19));
    }

}

