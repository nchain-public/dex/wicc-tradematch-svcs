package com.waykichain.dex.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QWiccTradeRequest is a Querydsl query type for WiccTradeRequest
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QWiccTradeRequest extends com.querydsl.sql.RelationalPathBase<WiccTradeRequest> {

    private static final long serialVersionUID = -185440192;

    public static final QWiccTradeRequest wiccTradeRequest = new QWiccTradeRequest("wicc_trade_request");

    public final NumberPath<java.math.BigDecimal> actualDealCoinAmount = createNumber("actualDealCoinAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> actualTradeAmount = createNumber("actualTradeAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amount = createNumber("amount", java.math.BigDecimal.class);

    public final NumberPath<Integer> blockHeight = createNumber("blockHeight", Integer.class);

    public final StringPath cancelTxHash = createString("cancelTxHash");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath dealCoinSymbol = createString("dealCoinSymbol");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> limitDealCoinAmount = createNumber("limitDealCoinAmount", java.math.BigDecimal.class);

    public final NumberPath<Integer> limitType = createNumber("limitType", Integer.class);

    public final StringPath marketCoinSymbol = createString("marketCoinSymbol");

    public final StringPath requestorWalletAddress = createString("requestorWalletAddress");

    public final NumberPath<Integer> requestPriceType = createNumber("requestPriceType", Integer.class);

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final NumberPath<java.math.BigDecimal> targetPrice = createNumber("targetPrice", java.math.BigDecimal.class);

    public final NumberPath<Integer> tradeDirection = createNumber("tradeDirection", Integer.class);

    public final StringPath tradePairCode = createString("tradePairCode");

    public final StringPath txHash = createString("txHash");

    public final NumberPath<Integer> txHeight = createNumber("txHeight", Integer.class);

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<WiccTradeRequest> primary = createPrimaryKey(id);

    public QWiccTradeRequest(String variable) {
        super(WiccTradeRequest.class, forVariable(variable), "null", "wicc_trade_request");
        addMetadata();
    }

    public QWiccTradeRequest(String variable, String schema, String table) {
        super(WiccTradeRequest.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QWiccTradeRequest(String variable, String schema) {
        super(WiccTradeRequest.class, forVariable(variable), schema, "wicc_trade_request");
        addMetadata();
    }

    public QWiccTradeRequest(Path<? extends WiccTradeRequest> path) {
        super(path.getType(), path.getMetadata(), "null", "wicc_trade_request");
        addMetadata();
    }

    public QWiccTradeRequest(PathMetadata metadata) {
        super(WiccTradeRequest.class, metadata, "null", "wicc_trade_request");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(actualDealCoinAmount, ColumnMetadata.named("actual_deal_coin_amount").withIndex(16).ofType(Types.DECIMAL).withSize(32).withDigits(8).notNull());
        addMetadata(actualTradeAmount, ColumnMetadata.named("actual_trade_amount").withIndex(14).ofType(Types.DECIMAL).withSize(32).withDigits(8).notNull());
        addMetadata(amount, ColumnMetadata.named("amount").withIndex(13).ofType(Types.DECIMAL).withSize(32).withDigits(8).notNull());
        addMetadata(blockHeight, ColumnMetadata.named("block_height").withIndex(7).ofType(Types.INTEGER).withSize(10));
        addMetadata(cancelTxHash, ColumnMetadata.named("cancel_tx_hash").withIndex(4).ofType(Types.VARCHAR).withSize(128));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(19).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(dealCoinSymbol, ColumnMetadata.named("deal_coin_symbol").withIndex(10).ofType(Types.VARCHAR).withSize(32));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(limitDealCoinAmount, ColumnMetadata.named("limit_deal_coin_amount").withIndex(15).ofType(Types.DECIMAL).withSize(32).withDigits(8).notNull());
        addMetadata(limitType, ColumnMetadata.named("limit_type").withIndex(17).ofType(Types.INTEGER).withSize(10));
        addMetadata(marketCoinSymbol, ColumnMetadata.named("market_coin_symbol").withIndex(9).ofType(Types.VARCHAR).withSize(32));
        addMetadata(requestorWalletAddress, ColumnMetadata.named("requestor_wallet_address").withIndex(6).ofType(Types.VARCHAR).withSize(64));
        addMetadata(requestPriceType, ColumnMetadata.named("request_price_type").withIndex(5).ofType(Types.INTEGER).withSize(10));
        addMetadata(status, ColumnMetadata.named("status").withIndex(18).ofType(Types.INTEGER).withSize(10));
        addMetadata(targetPrice, ColumnMetadata.named("target_price").withIndex(12).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(tradeDirection, ColumnMetadata.named("trade_direction").withIndex(11).ofType(Types.INTEGER).withSize(10));
        addMetadata(tradePairCode, ColumnMetadata.named("trade_pair_code").withIndex(8).ofType(Types.VARCHAR).withSize(64));
        addMetadata(txHash, ColumnMetadata.named("tx_hash").withIndex(2).ofType(Types.VARCHAR).withSize(128));
        addMetadata(txHeight, ColumnMetadata.named("tx_height").withIndex(3).ofType(Types.INTEGER).withSize(10));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(20).ofType(Types.TIMESTAMP).withSize(19));
    }

}

