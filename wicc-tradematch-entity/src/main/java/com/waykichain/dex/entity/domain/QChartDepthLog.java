package com.waykichain.dex.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QChartDepthLog is a Querydsl query type for ChartDepthLog
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QChartDepthLog extends com.querydsl.sql.RelationalPathBase<ChartDepthLog> {

    private static final long serialVersionUID = 863381116;

    public static final QChartDepthLog chartDepthLog = new QChartDepthLog("chart_depth_log");

    public final NumberPath<java.math.BigDecimal> count = createNumber("count", java.math.BigDecimal.class);

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath depth = createString("depth");

    public final NumberPath<Long> eoms = createNumber("eoms", Long.class);

    public final DateTimePath<java.util.Date> eosDate = createDateTime("eosDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public final StringPath sideCode = createString("sideCode");

    public final StringPath tradePairCode = createString("tradePairCode");

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> volAmount = createNumber("volAmount", java.math.BigDecimal.class);

    public final com.querydsl.sql.PrimaryKey<ChartDepthLog> primary = createPrimaryKey(id);

    public QChartDepthLog(String variable) {
        super(ChartDepthLog.class, forVariable(variable), "null", "chart_depth_log");
        addMetadata();
    }

    public QChartDepthLog(String variable, String schema, String table) {
        super(ChartDepthLog.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QChartDepthLog(String variable, String schema) {
        super(ChartDepthLog.class, forVariable(variable), schema, "chart_depth_log");
        addMetadata();
    }

    public QChartDepthLog(Path<? extends ChartDepthLog> path) {
        super(path.getType(), path.getMetadata(), "null", "chart_depth_log");
        addMetadata();
    }

    public QChartDepthLog(PathMetadata metadata) {
        super(ChartDepthLog.class, metadata, "null", "chart_depth_log");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(count, ColumnMetadata.named("count").withIndex(8).ofType(Types.DECIMAL).withSize(36).withDigits(18));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(10).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(depth, ColumnMetadata.named("depth").withIndex(6).ofType(Types.VARCHAR).withSize(16));
        addMetadata(eoms, ColumnMetadata.named("eoms").withIndex(3).ofType(Types.BIGINT).withSize(19));
        addMetadata(eosDate, ColumnMetadata.named("eos_date").withIndex(4).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(19).notNull());
        addMetadata(price, ColumnMetadata.named("price").withIndex(7).ofType(Types.DECIMAL).withSize(36).withDigits(18));
        addMetadata(sideCode, ColumnMetadata.named("side_code").withIndex(5).ofType(Types.VARCHAR).withSize(8));
        addMetadata(tradePairCode, ColumnMetadata.named("trade_pair_code").withIndex(2).ofType(Types.VARCHAR).withSize(32));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(11).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(volAmount, ColumnMetadata.named("vol_amount").withIndex(9).ofType(Types.DECIMAL).withSize(36).withDigits(18));
    }

}

