package com.waykichain.dex.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QWiccDexTransaction is a Querydsl query type for WiccDexTransaction
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QWiccDexTransaction extends com.querydsl.sql.RelationalPathBase<WiccDexTransaction> {

    private static final long serialVersionUID = -1336434500;

    public static final QWiccDexTransaction wiccDexTransaction = new QWiccDexTransaction("wicc_dex_transaction");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> transactedAtMs = createNumber("transactedAtMs", Long.class);

    public final StringPath txHash = createString("txHash");

    public final NumberPath<Integer> txHeight = createNumber("txHeight", Integer.class);

    public final NumberPath<Integer> txType = createNumber("txType", Integer.class);

    public final com.querydsl.sql.PrimaryKey<WiccDexTransaction> primary = createPrimaryKey(id);

    public QWiccDexTransaction(String variable) {
        super(WiccDexTransaction.class, forVariable(variable), "null", "wicc_dex_transaction");
        addMetadata();
    }

    public QWiccDexTransaction(String variable, String schema, String table) {
        super(WiccDexTransaction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QWiccDexTransaction(String variable, String schema) {
        super(WiccDexTransaction.class, forVariable(variable), schema, "wicc_dex_transaction");
        addMetadata();
    }

    public QWiccDexTransaction(Path<? extends WiccDexTransaction> path) {
        super(path.getType(), path.getMetadata(), "null", "wicc_dex_transaction");
        addMetadata();
    }

    public QWiccDexTransaction(PathMetadata metadata) {
        super(WiccDexTransaction.class, metadata, "null", "wicc_dex_transaction");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(transactedAtMs, ColumnMetadata.named("transacted_at_ms").withIndex(5).ofType(Types.BIGINT).withSize(19));
        addMetadata(txHash, ColumnMetadata.named("tx_hash").withIndex(3).ofType(Types.VARCHAR).withSize(128));
        addMetadata(txHeight, ColumnMetadata.named("tx_height").withIndex(2).ofType(Types.INTEGER).withSize(10));
        addMetadata(txType, ColumnMetadata.named("tx_type").withIndex(4).ofType(Types.INTEGER).withSize(10));
    }

}

