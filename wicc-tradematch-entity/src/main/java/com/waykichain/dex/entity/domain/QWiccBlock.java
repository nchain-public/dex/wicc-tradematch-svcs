package com.waykichain.dex.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QWiccBlock is a Querydsl query type for WiccBlock
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QWiccBlock extends com.querydsl.sql.RelationalPathBase<WiccBlock> {

    private static final long serialVersionUID = 2023583352;

    public static final QWiccBlock wiccBlock = new QWiccBlock("wicc_block");

    public final StringPath blockHash = createString("blockHash");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final NumberPath<Integer> height = createNumber("height", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nextBlockHash = createString("nextBlockHash");

    public final StringPath previousBlockHash = createString("previousBlockHash");

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<WiccBlock> primary = createPrimaryKey(id);

    public QWiccBlock(String variable) {
        super(WiccBlock.class, forVariable(variable), "null", "wicc_block");
        addMetadata();
    }

    public QWiccBlock(String variable, String schema, String table) {
        super(WiccBlock.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QWiccBlock(String variable, String schema) {
        super(WiccBlock.class, forVariable(variable), schema, "wicc_block");
        addMetadata();
    }

    public QWiccBlock(Path<? extends WiccBlock> path) {
        super(path.getType(), path.getMetadata(), "null", "wicc_block");
        addMetadata();
    }

    public QWiccBlock(PathMetadata metadata) {
        super(WiccBlock.class, metadata, "null", "wicc_block");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(blockHash, ColumnMetadata.named("block_hash").withIndex(3).ofType(Types.VARCHAR).withSize(128));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(6).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(height, ColumnMetadata.named("height").withIndex(2).ofType(Types.INTEGER).withSize(10));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(nextBlockHash, ColumnMetadata.named("next_block_hash").withIndex(5).ofType(Types.VARCHAR).withSize(128));
        addMetadata(previousBlockHash, ColumnMetadata.named("previous_block_hash").withIndex(4).ofType(Types.VARCHAR).withSize(128));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(7).ofType(Types.TIMESTAMP).withSize(19));
    }

}

