package com.waykichain.dex.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * WiccTradeRequest is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class WiccTradeRequest implements Serializable {

    @Column("actual_deal_coin_amount")
    private java.math.BigDecimal actualDealCoinAmount;

    @Column("actual_trade_amount")
    private java.math.BigDecimal actualTradeAmount;

    @Column("amount")
    private java.math.BigDecimal amount;

    @Column("block_height")
    private Integer blockHeight;

    @Column("cancel_tx_hash")
    private String cancelTxHash;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("deal_coin_symbol")
    private String dealCoinSymbol;

    @Column("id")
    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column("limit_deal_coin_amount")
    private java.math.BigDecimal limitDealCoinAmount;

    @Column("limit_type")
    private Integer limitType;

    @Column("market_coin_symbol")
    private String marketCoinSymbol;

    @Column("requestor_wallet_address")
    private String requestorWalletAddress;

    @Column("request_price_type")
    private Integer requestPriceType;

    @Column("status")
    private Integer status;

    @Column("target_price")
    private java.math.BigDecimal targetPrice;

    @Column("trade_direction")
    private Integer tradeDirection;

    @Column("trade_pair_code")
    private String tradePairCode;

    @Column("tx_hash")
    private String txHash;

    @Column("tx_height")
    private Integer txHeight;

    @Column("updated_at")
    private java.util.Date updatedAt;

    public java.math.BigDecimal getActualDealCoinAmount() {
        return actualDealCoinAmount;
    }

    public void setActualDealCoinAmount(java.math.BigDecimal actualDealCoinAmount) {
        this.actualDealCoinAmount = actualDealCoinAmount;
    }

    public java.math.BigDecimal getActualTradeAmount() {
        return actualTradeAmount;
    }

    public void setActualTradeAmount(java.math.BigDecimal actualTradeAmount) {
        this.actualTradeAmount = actualTradeAmount;
    }

    public java.math.BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getBlockHeight() {
        return blockHeight;
    }

    public void setBlockHeight(Integer blockHeight) {
        this.blockHeight = blockHeight;
    }

    public String getCancelTxHash() {
        return cancelTxHash;
    }

    public void setCancelTxHash(String cancelTxHash) {
        this.cancelTxHash = cancelTxHash;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getDealCoinSymbol() {
        return dealCoinSymbol;
    }

    public void setDealCoinSymbol(String dealCoinSymbol) {
        this.dealCoinSymbol = dealCoinSymbol;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.math.BigDecimal getLimitDealCoinAmount() {
        return limitDealCoinAmount;
    }

    public void setLimitDealCoinAmount(java.math.BigDecimal limitDealCoinAmount) {
        this.limitDealCoinAmount = limitDealCoinAmount;
    }

    public Integer getLimitType() {
        return limitType;
    }

    public void setLimitType(Integer limitType) {
        this.limitType = limitType;
    }

    public String getMarketCoinSymbol() {
        return marketCoinSymbol;
    }

    public void setMarketCoinSymbol(String marketCoinSymbol) {
        this.marketCoinSymbol = marketCoinSymbol;
    }

    public String getRequestorWalletAddress() {
        return requestorWalletAddress;
    }

    public void setRequestorWalletAddress(String requestorWalletAddress) {
        this.requestorWalletAddress = requestorWalletAddress;
    }

    public Integer getRequestPriceType() {
        return requestPriceType;
    }

    public void setRequestPriceType(Integer requestPriceType) {
        this.requestPriceType = requestPriceType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public java.math.BigDecimal getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(java.math.BigDecimal targetPrice) {
        this.targetPrice = targetPrice;
    }

    public Integer getTradeDirection() {
        return tradeDirection;
    }

    public void setTradeDirection(Integer tradeDirection) {
        this.tradeDirection = tradeDirection;
    }

    public String getTradePairCode() {
        return tradePairCode;
    }

    public void setTradePairCode(String tradePairCode) {
        this.tradePairCode = tradePairCode;
    }

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    public Integer getTxHeight() {
        return txHeight;
    }

    public void setTxHeight(Integer txHeight) {
        this.txHeight = txHeight;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}

