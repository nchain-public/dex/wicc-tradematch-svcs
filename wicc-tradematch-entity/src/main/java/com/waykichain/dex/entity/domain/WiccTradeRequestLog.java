package com.waykichain.dex.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * WiccTradeRequestLog is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class WiccTradeRequestLog implements Serializable {

    @Column("from_actual_amount")
    private java.math.BigDecimal fromActualAmount;

    @Column("from_status")
    private Integer fromStatus;

    @Column("id")
    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column("request_id")
    private Long requestId;

    @Column("status")
    private Integer status;

    @Column("to_actual_amount")
    private java.math.BigDecimal toActualAmount;

    @Column("to_status")
    private Integer toStatus;

    public java.math.BigDecimal getFromActualAmount() {
        return fromActualAmount;
    }

    public void setFromActualAmount(java.math.BigDecimal fromActualAmount) {
        this.fromActualAmount = fromActualAmount;
    }

    public Integer getFromStatus() {
        return fromStatus;
    }

    public void setFromStatus(Integer fromStatus) {
        this.fromStatus = fromStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public java.math.BigDecimal getToActualAmount() {
        return toActualAmount;
    }

    public void setToActualAmount(java.math.BigDecimal toActualAmount) {
        this.toActualAmount = toActualAmount;
    }

    public Integer getToStatus() {
        return toStatus;
    }

    public void setToStatus(Integer toStatus) {
        this.toStatus = toStatus;
    }

}

