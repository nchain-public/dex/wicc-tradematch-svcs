package com.waykichain.dex.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QWiccTradeRequestLog is a Querydsl query type for WiccTradeRequestLog
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QWiccTradeRequestLog extends com.querydsl.sql.RelationalPathBase<WiccTradeRequestLog> {

    private static final long serialVersionUID = -1120740636;

    public static final QWiccTradeRequestLog wiccTradeRequestLog = new QWiccTradeRequestLog("wicc_trade_request_log");

    public final NumberPath<java.math.BigDecimal> fromActualAmount = createNumber("fromActualAmount", java.math.BigDecimal.class);

    public final NumberPath<Integer> fromStatus = createNumber("fromStatus", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> requestId = createNumber("requestId", Long.class);

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final NumberPath<java.math.BigDecimal> toActualAmount = createNumber("toActualAmount", java.math.BigDecimal.class);

    public final NumberPath<Integer> toStatus = createNumber("toStatus", Integer.class);

    public final com.querydsl.sql.PrimaryKey<WiccTradeRequestLog> primary = createPrimaryKey(id);

    public QWiccTradeRequestLog(String variable) {
        super(WiccTradeRequestLog.class, forVariable(variable), "null", "wicc_trade_request_log");
        addMetadata();
    }

    public QWiccTradeRequestLog(String variable, String schema, String table) {
        super(WiccTradeRequestLog.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QWiccTradeRequestLog(String variable, String schema) {
        super(WiccTradeRequestLog.class, forVariable(variable), schema, "wicc_trade_request_log");
        addMetadata();
    }

    public QWiccTradeRequestLog(Path<? extends WiccTradeRequestLog> path) {
        super(path.getType(), path.getMetadata(), "null", "wicc_trade_request_log");
        addMetadata();
    }

    public QWiccTradeRequestLog(PathMetadata metadata) {
        super(WiccTradeRequestLog.class, metadata, "null", "wicc_trade_request_log");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(fromActualAmount, ColumnMetadata.named("from_actual_amount").withIndex(3).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(fromStatus, ColumnMetadata.named("from_status").withIndex(5).ofType(Types.INTEGER).withSize(10));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(requestId, ColumnMetadata.named("request_id").withIndex(2).ofType(Types.BIGINT).withSize(19));
        addMetadata(status, ColumnMetadata.named("status").withIndex(7).ofType(Types.INTEGER).withSize(10));
        addMetadata(toActualAmount, ColumnMetadata.named("to_actual_amount").withIndex(4).ofType(Types.DECIMAL).withSize(32).withDigits(8));
        addMetadata(toStatus, ColumnMetadata.named("to_status").withIndex(6).ofType(Types.INTEGER).withSize(10));
    }

}

