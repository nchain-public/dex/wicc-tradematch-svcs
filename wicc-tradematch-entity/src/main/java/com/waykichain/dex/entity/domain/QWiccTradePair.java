package com.waykichain.dex.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QWiccTradePair is a Querydsl query type for WiccTradePair
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QWiccTradePair extends com.querydsl.sql.RelationalPathBase<WiccTradePair> {

    private static final long serialVersionUID = -271109431;

    public static final QWiccTradePair wiccTradePair = new QWiccTradePair("wicc_trade_pair");

    public final StringPath code = createString("code");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath dealCoinSymbol = createString("dealCoinSymbol");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath marketCoinSymbol = createString("marketCoinSymbol");

    public final NumberPath<Integer> onShelf = createNumber("onShelf", Integer.class);

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<WiccTradePair> primary = createPrimaryKey(id);

    public QWiccTradePair(String variable) {
        super(WiccTradePair.class, forVariable(variable), "null", "wicc_trade_pair");
        addMetadata();
    }

    public QWiccTradePair(String variable, String schema, String table) {
        super(WiccTradePair.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QWiccTradePair(String variable, String schema) {
        super(WiccTradePair.class, forVariable(variable), schema, "wicc_trade_pair");
        addMetadata();
    }

    public QWiccTradePair(Path<? extends WiccTradePair> path) {
        super(path.getType(), path.getMetadata(), "null", "wicc_trade_pair");
        addMetadata();
    }

    public QWiccTradePair(PathMetadata metadata) {
        super(WiccTradePair.class, metadata, "null", "wicc_trade_pair");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(code, ColumnMetadata.named("code").withIndex(2).ofType(Types.VARCHAR).withSize(64));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(6).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(dealCoinSymbol, ColumnMetadata.named("deal_coin_symbol").withIndex(5).ofType(Types.VARCHAR).withSize(32));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(marketCoinSymbol, ColumnMetadata.named("market_coin_symbol").withIndex(4).ofType(Types.VARCHAR).withSize(32));
        addMetadata(onShelf, ColumnMetadata.named("on_shelf").withIndex(3).ofType(Types.INTEGER).withSize(10));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(7).ofType(Types.TIMESTAMP).withSize(19));
    }

}

