package com.waykichain.dex.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * WiccTradePair is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class WiccTradePair implements Serializable {

    @Column("code")
    private String code;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("deal_coin_symbol")
    private String dealCoinSymbol;

    @Column("id")
    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column("market_coin_symbol")
    private String marketCoinSymbol;

    @Column("on_shelf")
    private Integer onShelf;

    @Column("updated_at")
    private java.util.Date updatedAt;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getDealCoinSymbol() {
        return dealCoinSymbol;
    }

    public void setDealCoinSymbol(String dealCoinSymbol) {
        this.dealCoinSymbol = dealCoinSymbol;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarketCoinSymbol() {
        return marketCoinSymbol;
    }

    public void setMarketCoinSymbol(String marketCoinSymbol) {
        this.marketCoinSymbol = marketCoinSymbol;
    }

    public Integer getOnShelf() {
        return onShelf;
    }

    public void setOnShelf(Integer onShelf) {
        this.onShelf = onShelf;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}

