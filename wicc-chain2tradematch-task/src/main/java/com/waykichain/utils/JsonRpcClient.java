package com.waykichain.utils;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class JsonRpcClient {

    private ObjectMapper mapper;
    private String jsonRpcUrl;
    private Boolean isJsonFormat = true;
    private Logger log= LoggerFactory.getLogger(this.getClass());


    private CloseableHttpClient httpClient;

    public JsonRpcClient(String jsonRpcUrl,
                         Boolean isJsonFormat) {
        super();
        this.jsonRpcUrl = jsonRpcUrl;
        this.mapper = new ObjectMapper();
        this.isJsonFormat = isJsonFormat;
        this.httpClient = HttpClients.createDefault();

    }

    public <U> U execute(Object dataIn, Class<U> classOut) throws IOException {
        String json = this.mapper.writeValueAsString(dataIn);
        HttpPost post = new HttpPost(jsonRpcUrl);
        StringEntity body = new StringEntity(json);
        post.setEntity(body);
        if(this.isJsonFormat ) {
            post.setHeader("Accept", "application/json");
            post.setHeader("Content-type", "application/json");
        }

        log.info("request:"+json);
        CloseableHttpResponse response = httpClient.execute(post);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line;
        StringBuilder sb = new StringBuilder();

        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        log.info("response:"+sb.toString());
        return this.mapper.readValue(sb.toString(), classOut);
    }

    public <U> U executeJson(Object dataIn, Class<U> classOut) throws IOException {
        String json = this.mapper.writeValueAsString(dataIn);
        HttpPost post = new HttpPost(jsonRpcUrl);
        StringEntity body = new StringEntity(json);
        post.setEntity(body);
        if(this.isJsonFormat ) {
            post.setHeader("Accept", "application/json");
            post.setHeader("Content-type", "application/json");
        }


        CloseableHttpResponse response = httpClient.execute(post);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line;
        StringBuilder sb = new StringBuilder();

        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }

        return JSON.parseObject(sb.toString(), classOut);
    }


    public <T> T executeGet(Class<T> classOut) throws IOException{

        HttpGet httpGet = new HttpGet(jsonRpcUrl);
        if (this.isJsonFormat) {
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
        }

        CloseableHttpResponse response = httpClient.execute(httpGet);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line;
        StringBuilder sb = new StringBuilder();

        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }

        return JSON.parseObject(sb.toString(), classOut);

    }

}
