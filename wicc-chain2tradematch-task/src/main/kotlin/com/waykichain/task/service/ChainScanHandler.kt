package com.waykichain.task.service

import com.waykichain.biz.redis.repository.impl.ValueCacheRedisRepository
import com.waykichain.chainstarter.service.WaykichainCoinHandler
import com.waykichain.coin.wicc.vo.WiccBlockResult
import com.waykichain.dex.base.consts.DexRedisKey
import com.waykichain.dex.commons.xservice.handler.DexTransactionHandler
import com.waykichain.dex.commons.xservice.handler.DexTransactionHandlerManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 *  Created by yehuan on 2019/6/19
 */

@Service
class ChainScanHandler{

    fun scan(){

        val localHeight = getLocalHeight()
        val chainHeight = getChainSyncHeight()

        for(i in localHeight+1.. chainHeight){
            val block = handler.getBlockByHeight(i)
            handleTradeRequest(block)
        }

    }

    fun handleTradeRequest(block: WiccBlockResult){

        val txHashs = block.tx
        for( hash in txHashs){
            val resp =  handler.getTxDetailJson(hash)
            dexTransactionHandlerManager.handle(resp)
        }
    }

    fun getChainSyncHeight(): Int{
        try {
            return handler.getChainInfo()!!.result.getSyncblockheight()
        }catch (e: Exception){
            return 0
        }
    }

    fun getLocalHeight(): Int{
        try {
            return valueCacheRedisRepository.get(DexRedisKey.DEX_CHAIN_HEIGHT_REDIS_KEY)!!.toInt()
        }catch (e: Exception){
            return 2800000
        }
    }

    @Autowired lateinit var dexTransactionHandlerManager: DexTransactionHandlerManager
    @Autowired
    lateinit var valueCacheRedisRepository: ValueCacheRedisRepository

    @Autowired
    lateinit var handler: WaykichainCoinHandler

}