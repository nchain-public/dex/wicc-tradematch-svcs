package com.waykichain

import com.waykichain.commons.util.BaseEnv
import org.springframework.boot.SpringApplication
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.annotation.ComponentScan

@SpringBootConfiguration
@ComponentScan(basePackages = arrayOf("com.waykichain"))
@EnableCaching
@EnableAutoConfiguration
open class Application

fun main(args: Array<String>){
    BaseEnv.init()
    val app = SpringApplication(Application::class.java)
    app.isWebEnvironment = false
    app.setApplicationContextClass(AnnotationConfigApplicationContext::class.java)
    app.run(*args)
}